# CityMobile

Creation KEY:
keytool -genkey -v -keystore citymagine.keystore -alias citymagine -keyalg RSA -keysize 2048 -validity 10000

Sign APK:
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore citymagine.keystore app-release-unsigned.apk citymagine
